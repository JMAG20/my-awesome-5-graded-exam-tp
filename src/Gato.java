public class Gato {
    private String nombre;
    private String id;
    private int vidas;

    public Gato(String nombre, String id, int vidas) {
        this.nombre = nombre;
        this.id = id;
        this.vidas = vidas;
    }

    public void maullar(){
        System.out.print(" Meow, Miau, Nya");
    }

    public void dormir(){
        System.out.println("Zzzzz...");
    }
    @Override
    public String toString() {
        return "Me llamo: " + nombre +" con Id: "+ id + " Vidas: " + vidas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getVidas() {
        return vidas;
    }

    public void setVidas(int vidas) {
        this.vidas = vidas;
    }
}
