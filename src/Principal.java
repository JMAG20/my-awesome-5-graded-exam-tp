import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Principal {
    public static void main(String[] args) {
        int[] numeros = {0,1,2,3,4,5,6,7,8,9};
        ArrayList<Integer> nums = new ArrayList<>();
        nums.add(3); nums.add(2); nums.add(1); nums.add(4); nums.add(6); nums.add(5);
        int sumaForEach = 0;
        String elementos = "(";
        for(int num : numeros){
            sumaForEach+=num;
            elementos=elementos.concat(num+",");
        }
        elementos=elementos.substring(0,elementos.length()-1)+")";
        System.out.println("Los elementos de la lista numeros son:\n"+elementos);
        Iterator<Integer> iterator = nums.iterator();
        int sumaIterator=0;
        while(iterator.hasNext()){
            Integer numero = iterator.next();
            sumaIterator+=numero;
        }

        System.out.println("La suma for each es:"+sumaForEach);
        System.out.println("\nLos elementos de la lista nums son:");
        nums.forEach((num) -> {
            System.out.println(num);
        } );
        System.out.println("La suma Iterator es: "+sumaIterator);

        Gato gato1 = new Gato("Oden","8153A",7);
        Gato gato2 = new Gato("Roger","9173B",3);
        Gato gato3 = new Gato("Rocky","6451C",9);
        List<Gato> gatos = new ArrayList<>();
        gatos.add(gato1);
        gatos.add(gato3);
        gatos.add(gato2);

        System.out.println("\nGatos por id:");
        Iterator<Gato> iterator1 = gatos.iterator();
        while (iterator1.hasNext()){
            String idAct = iterator1.next().getId();
            System.out.println("ID: "+idAct);
        }
        System.out.println();
        for(Gato neko : gatos){
            if(neko.getVidas()<4){
                System.out.println(neko.getNombre()+" es un bakeneko.");
            }else System.out.println(neko.getNombre()+" es un buen gato.");
        }
        gatos.forEach((gato)->{
            System.out.print("\n"+gato);
            gato.maullar();
        });
    }
}
